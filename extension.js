/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
 
import Clutter from 'gi://Clutter';
 
import {Extension, InjectionManager} from 'resource:///org/gnome/shell/extensions/extension.js';
//import * as Main from 'resource:///org/gnome/shell/ui/main.js';
//import * as PanelMenu from 'resource:///org/gnome/shell/ui/panelMenu.js';
import * as MessageTray from 'resource:///org/gnome/shell/ui/messageTray.js';
import * as MessageList from 'resource:///org/gnome/shell/ui/messageList.js';

export default class PlainExampleExtension extends Extension {
    enable() {
        this._injectionManager = new InjectionManager();
        this._settings = this.getSettings();


        this._injectionManager.overrideMethod(MessageTray.MessageTray.prototype, '_expandBanner',
            originalMethod => {
                return function (autoExpanding) {
                    if (!autoExpanding)
                     this._ensureBannerFocused();
                };
            });
            
        this._injectionManager.overrideMethod(MessageTray.MessageTray.prototype, '_showNotification',
            originalMethod => {
                const settings = this._settings;
                                    
                return function () {
                    originalMethod.call(this);
                    const banner = this._banner;
                    banner._header.expandButton.visible = true;
                    
                    const value = settings.get_boolean('always-show-buttons');
                    banner._actionBin.visible = banner._actionBin.child && value;
                    banner._actionBin.scale_y = value ? 1 : 0;
                    banner._alwaysShowButtons = value;
                };
            }); 
            
        this._injectionManager.overrideMethod(MessageList.Message.prototype, 'expand',
            originalMethod => {
                return function (animate) {
                    this.expanded = true;

                    this._actionBin.visible = !!this._actionBin.child;

                    const duration = animate ? MessageTray.ANIMATION_TIME : 0;
                    this._bodyBin.ease_property('@layout.expansion', 1, {
                        progress_mode: Clutter.AnimationMode.EASE_OUT_QUAD,
                        duration,
                    });

                    if (!this._alwaysShowButtons) {
                        this._actionBin.scale_y = 0;
                        this._actionBin.ease({
                            scale_y: 1,
                            duration,
                            mode: Clutter.AnimationMode.EASE_OUT_QUAD,
                        });
                    }

                    this._header.expandButton.ease({
                        rotation_angle_z: 180,
                        duration,
                    });

                    this.emit('expanded');
                };
            }); 
            
          this._injectionManager.overrideMethod(MessageList.Message.prototype, 'unexpand',
            originalMethod => {
                return function (animate) {
                    const duration = animate ? MessageTray.ANIMATION_TIME : 0;
                    this._bodyBin.ease_property('@layout.expansion', 0, {
                        progress_mode: Clutter.AnimationMode.EASE_OUT_QUAD,
                        duration,
                        onComplete: () => {
                            this.expanded = false;
                        }
                    });

                    if (!this._alwaysShowButtons) {
                        this._actionBin.ease({
                            scale_y: 0,
                            duration,
                            mode: Clutter.AnimationMode.EASE_OUT_QUAD,
                            onComplete: () => {
                                this._actionBin.hide();
                            },
                        });
                    }

                    this._header.expandButton.ease({
                        rotation_angle_z: 0,
                        duration,
                    });

                    this.emit('unexpanded');
                };
            }); 
            
          this._injectionManager.overrideMethod(MessageList.Message.prototype, '_updateExpandButton',
            originalMethod => {
                return function () {
                    if (!this._bodyLabel.has_allocation())
                      return;
                    const layout = this._bodyLabel.clutter_text.get_layout();
                    const canExpand = layout.is_ellipsized() || this.expanded || (!!this._actionBin.child && !this._alwaysShowButtons);
                    // Use opacity to not trigger a relayout
                    this._header.expandButton.opacity = canExpand ? 255 : 0;
                };
            }); 
    }

    disable() {
        this._injectionManager.clear();
        this._injectionManager = null;
        this._settings = null;
    }
/*
    enable() {
        // Create a panel button
        this._indicator = new PanelMenu.Button(0.0, this.metadata.name, false);

        // Add an icon
        const icon = new St.Icon({
            icon_name: 'face-laugh-symbolic',
            style_class: 'system-status-icon',
        });
        this._indicator.add_child(icon);

        // Add the indicator to the panel
        Main.panel.addToStatusArea(this.uuid, this._indicator);
    }

    disable() {
        this._indicator?.destroy();
        this._indicator = null;
    }
    */
}
