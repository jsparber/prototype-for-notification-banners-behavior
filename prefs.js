import Gio from 'gi://Gio';
import Adw from 'gi://Adw';

import {ExtensionPreferences, gettext as _} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';


export default class ExamplePreferences extends ExtensionPreferences {
    fillPreferencesWindow(window) {
        // Create a preferences page, with a single group
        const page = new Adw.PreferencesPage({
            title: _('General'),
            icon_name: 'dialog-information-symbolic',
        });
        window.add(page);

        const group = new Adw.PreferencesGroup({
            title: _('Behavior'),
            description: _('Configure the expand behavior for notification banners'),
        });
        page.add(group);

        // Create a new preferences row
        const row = new Adw.SwitchRow({
            title: _('Always show buttons'),
            subtitle: _('Whether to show buttons in a notification banner even when not expanded'),
        });
        group.add(row);

        window._settings = this.getSettings();
        window._settings.bind('always-show-buttons', row, 'active',
            Gio.SettingsBindFlags.DEFAULT);
    }
}
