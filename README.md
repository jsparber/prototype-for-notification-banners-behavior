Prototype to test dropping expand behavior
==========================================

This implements the behavior discussed in https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/7713

This extension has an option to always show buttons in a banner or only when the notification banner is expanded.

### Usage

```
wget https://gitlab.gnome.org/jsparber/prototype-for-notification-banners-behavior/-/archive/main/prototype-for-notification-banners-behavior-main.zip
gnome-extensions install prototype-for-notification-banners-behavior-main.zip
gnome-extensions enable drophoverexpand@juliansparber.com

# To change preferences run
gnome-extensions prefs drophoverexpand@juliansparber.com

```